---
format: markdown
categories: project extension
toc: yes
author:
- Michael Ambrus <michael@axis.com>
title: Integrate Lua in any project
---

* `CMake` build-wrapper for `lua` and `luac`
* Extend Lua with a terminal
  * Terminal can be attached via `TCP`-sockets, `UARTS` which makes it
    highly suitable for constrained embedded systems. No need for Embedded
    Lua iow (a Lua fork) ad is actually discouraged as is deviates from the
    language and makes file-based I/O very cumersome.
* Terminal invocations execute in threads, one VM per session
  * I.e. VM's run independently of each other but can share data,
    synchronize and use the same C bridge-functions.

## Reference projects:

* `Fanband` - Resource constrained (32K RAM) wrist-worn
   [`PMOLED`](https://en.wikipedia.org/wiki/OLED) based personal device with
   emphasis on embedded `OpenGL` and vectorized fonts.
* `Liblog`  - High accuracy SW logger/tracer that can have it's trace-points
  controlled by logic running in Lua without support from OS and with higher
  temporal accuracy and negligible run-time impact.
* [`Ethos U`](https://developer.arm.com/documentation/101888/0500/Introduction/Ethos-U-system),
  as development support engine for `ARM ML`'s first gen `NPU` (embedded
  machine learning device). Used interactively by developers for live tuning
  ,which would otherwise require a longer change-build-launch workflow, and in `CI`
  test-framework for executing featurers, making settings automated using
  simple host-side scriptable text-based `RPC`


